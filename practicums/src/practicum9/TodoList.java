package practicum9;

import lib.TextIO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by mkunnapa on 24.10.2016.
 */
public class TodoList {
    public static final String FILE_NAME = "todo.txt";
    static ArrayList<String> todoItems = new ArrayList<>();

    public static void main(String[] args) {
        loadTodoList();
        modifyTodoList();
        saveTodoList();
    }

    private static void saveTodoList() {
        FileManager.writeToFile(todoItems, FILE_NAME);
    }

    private static void loadTodoList() {
        try {
            todoItems = FileManager.readFile(FILE_NAME);
        } catch (FileNotFoundException e) {
            FileManager.writeToFile(new ArrayList<String>(), FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void modifyTodoList() {
        while (true) {
            printTodo();
            System.out.println("Please input a new item, an index to delete, or an empty line to exit");
            String input = TextIO.getlnString();
            if (input.isEmpty()) {
                return;
            }
            if (isInteger(input)) {
                try {
                    todoItems.remove(Integer.parseInt(input));
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("There is no item with the index " + input);
                    continue;
                }
            } else {
                todoItems.add(input);
            }
        }
    }

    private static void printTodo() {
        System.out.println("TODO:");
        int counter = 0;
        for (String todoItem : todoItems) {
            System.out.printf("%d %s\n", counter++, todoItem);
        }
    }

    private static boolean isInteger(String string) {
        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }


}
