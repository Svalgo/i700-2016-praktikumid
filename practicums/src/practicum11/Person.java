package practicum11;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mihkel on 06.11.2016. Based on Java SE 8: Lambda Quick Start
 */
public class Person {
    private String givenName;
    private String surName;
    private int age;
    private Gender gender;
    private String eMail;
    private String phone;
    private String address;

    public static ArrayList<Person> callATeam() {
        ArrayList<Person> people = new ArrayList<>();

        Person p1 = new Person();
        p1.givenName = "John";
        p1.surName = "Smith";
        p1.age = 54;
        p1.gender = Gender.MALE;
        p1.eMail = "Hannibal@ateam.org";
        p1.phone = "5554545";
        p1.address = "Unknown";
        people.add(p1);

        Person p2 = new Person();
        p2.givenName = "Templeton";
        p2.surName = "Peck";
        p2.age = 38;
        p2.gender = Gender.MALE;
        p2.eMail = "Faceman@ateam.org";
        p2.phone = "5554545";
        p2.address = "Unknown";
        people.add(p2);

        Person p3 = new Person();
        p3.givenName = "Bosco";
        p3.surName = "Baracus";
        p3.age = 33;
        p3.gender = Gender.MALE;
        p3.eMail = "BA@ateam.org";
        p3.phone = "5554545";
        p3.address = "Unknown";
        people.add(p3);

        Person p4 = new Person();
        p4.givenName = "H.M.";
        p4.surName = "Murdock";
        p4.age = 36;
        p4.gender = Gender.MALE;
        p4.eMail = "HowlingMad@ateam.org";
        p4.phone = "5554545";
        p4.address = "Unknown";
        people.add(p4);

        Person p5 = new Person();
        p5.givenName = "Amy Amanda";
        p5.surName = "Allen";
        p5.age = 28;
        p5.gender = Gender.FEMALE;
        p5.eMail = "Amy@ateam.org";
        p5.phone = "5554545";
        p5.address = "Unknown";
        people.add(p5);

        return people;
    }

    public void printName() {
        System.out.println(givenName + " " + surName);
    }

    /*
     * Accessors
     */
    public String getGivenName() {
        return givenName;
    }

    public String getSurName() {
        return surName;
    }

    public int getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    public String geteMail() {
        return eMail;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }


}
