package practicum10;

import org.junit.Assert;

import java.io.File;
import java.util.Arrays;

/**
 * Created by mkunnapa on 31.10.2016.
 */
public class Exercises {
    public static void main(String[] args) {
        int[] row1 = {4, 6, 1, 9};
        int[] row2 = {-5, -10, -12};
        int[] row3 = {0, 2, 1};

        int[][] matrix = {
                row1, // max: 9
                row2, // max: -5
                row3, // max: 2
        };
        //Sum = 9 + (-5) + 2 = 6

        /*Assert.assertEquals(9, maxOfRow(row1));
        Assert.assertEquals(-5, maxOfRow(row2));
        Assert.assertEquals(2, maxOfRow(row3));*/
//        Assert.assertEquals(6, sumOfRowMax(matrix));
        File folder = new File("H:\\");
        listFiles(folder);
    }

    public static int sumOfRowMax(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            sum += maxOfRow(matrix[i]);
        }
        return sum;
    }

    public static int maxOfRow(int[] row) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < row.length; i++) {
            if (row[i] > max) {
                max = row[i];
            }
        }
        return max;
    }

    public static void listFiles(File folder) {
        if (folder != null || folder.listFiles() != null) {
            for (File file : folder.listFiles()) {
                if (file.isDirectory()) {
                    listFiles(file);
                }
                if (file.getName().endsWith(".java")) {
                    System.out.println(file);
                }
            }
        }
    }

}
