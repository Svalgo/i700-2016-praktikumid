package practicum4;

public class Even {
    public static void main(String[] args) {
        //Print out even numbers on one line from 0 to 10 (0,2,4 etc)
        //0 2 4 6 8 10
        //Again, think what should be the beginning value for the variable

        for (int i = 0; i < 11; i += 2) {
            System.out.print(i + " ");
        }

//        for (int i = 0; i < 11; i++) {
//            boolean isEven = i % 2 == 0;
//            if (isEven) {
//                System.out.print(i + " ");
//            }
//        }
    }
}
