package practicum4;

public class ZeroToNineTable {
    public static void main(String[] args) {
        /*
        0 1 2 3 4 5 6 7 8 9
        1 2 3 4 5 6 7 8 9 0
        2 3 4 5 6 7 8 9 0 1
        3 4 5 6 7 8 9 0 1 2
        4 5 6 7 8 9 0 1 2 3
        5 6 7 8 9 0 1 2 3 4
        6 7 8 9 0 1 2 3 4 5
        7 8 9 0 1 2 3 4 5 6
        8 9 0 1 2 3 4 5 6 7
        9 0 1 2 3 4 5 6 7 8
         */

        for (int row = 0; row < 10; row++) {
            for (int col = 0; col < 10; col++) {
                int output = row + col;
                if (output > 9) {
                    output-=10;
                }
                System.out.print(output + " ");
            }
            System.out.println();
        }
    }
}
