package practicum4;

/**
 * Created by Mihkel on 24.09.2016.
 */
public class PinCodeAlternative {
    public static void main(String[] args) {
        for (int i = 0; i < 10000; i++) {
            System.out.printf("%04d \n", i);
        }
    }
}
