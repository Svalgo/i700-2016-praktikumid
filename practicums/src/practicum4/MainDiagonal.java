package practicum4;

import lib.TextIO;

public class MainDiagonal {
    public static void main(String[] args) {
        /*
        1 0 0 0 0 0 0
        0 1 0 0 0 0 0
        0 0 1 0 0 0 0
        0 0 0 1 0 0 0
        0 0 0 0 1 0 0
        0 0 0 0 0 1 0
        0 0 0 0 0 0 1
         */
        
        //Get user input
        System.out.println("Hello, user, how big should the table be?");
        int size = TextIO.getInt();
        
        //Create table
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                if (row == col) {
                    System.out.print("1 ");
                } else {
                    System.out.print("0 ");
                }
//                System.out.print("R:" + row + "C:" + col +" ");
            }
            System.out.println();
        }
    }
}
