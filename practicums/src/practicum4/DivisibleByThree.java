package practicum4;

public class DivisibleByThree {
    public static void main(String[] args) {
        //Print out numbers that are divisible by 3 from 30 to 0 on one line (30, 27, 24, 21 etc)
        //30 27 24 21 18 15 12 9 6 3 0

//        for (int i = 30; i >= 0; i--) {
//            if (i % 3 == 0) {
//                System.out.print(i + " ");
//            }
//        }

        for (int i = 30; i >= 0 ; i -= 3) {
            System.out.print(i + " ");
        }

        /*
        int i = 0;
        i = i + 1;

        i++;
        i--;

        i += 1;
        i -= 5;
        i *= 13;
        i /= 5;
         */
    }
}
