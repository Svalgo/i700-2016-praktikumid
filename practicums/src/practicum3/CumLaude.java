package practicum3;

import lib.TextIO;

public class CumLaude {
    //Write a program that asks the user for the average grade and the grade for diploma thesis.
    // Then it prints out whether a student with these results would be given Cum Laude.
    // Cum Laude is given only when average grade is greater than 4.5 and grade for diploma thesis is 5.
    public static void main(String[] args) {
        //Ask the user for their average grade
        System.out.println("Please enter your average grade");
        double averageGrade = TextIO.getDouble();

        boolean averageGradeValid = false;
        //check if the input is valid
        if (averageGrade >= 0. && averageGrade <= 5.) {
            averageGradeValid = true;
        }

        //Ask for diploma thesis grade
        System.out.println("Please enter your diploma thesis grade");
        double thesisGrade = TextIO.getDouble();

        boolean thesisGradeValid = false;
        //check if the input is valid
        if (thesisGrade >= 0. && averageGrade <= 5.) {
            thesisGradeValid = true;
        }

        if (averageGradeValid && thesisGradeValid) {
            boolean averageHighEnough = averageGrade > 4.5;
            boolean thesisGradeIsFive = thesisGrade == 5;
            //check if user eligible for cum laude
            if (averageHighEnough && thesisGradeIsFive) {
                System.out.println("You are eligible for Cum Laude");
            } else {
                System.out.println("You are not eligible for Cum Laude");
            }
        } else {
            System.out.println("Your input was invalid, average grade and thesis grade need to be positive");
        }

    }
}
