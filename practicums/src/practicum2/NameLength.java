package practicum2;

import lib.TextIO;

/**
 * Created by mkunnapa on 05.09.2016.
 */
public class NameLength {
    public static void main(String[] args) {
        String name = TextIO.getlnString();
        int nameLength = name.length();
        System.out.println(nameLength);
    }
}
