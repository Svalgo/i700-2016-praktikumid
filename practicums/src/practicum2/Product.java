package practicum2;

import lib.TextIO;

public class Product {
    public static void main(String[] args) {
        System.out.println("Please enter the first number:");
        //Store the number
        int firstNumber = TextIO.getlnInt();
        //Input number 2
        System.out.println("Please enter the second number:");
        //Store the number
        int secondNumber = TextIO.getlnInt();
        //Print out the product of the numbers
        int product = firstNumber * secondNumber;
        System.out.println("The product of the numbers " + firstNumber + " and " + secondNumber + " is " + product);
    }
}
