package practicum5;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExercisesTest {

    @Test
    public void xCharacters() throws Exception {
        String result = Exercises.xCharacters('a', 5);
        String expected = "aaaaa";
        Assert.assertEquals(expected, result);
    }

}