package practicum7;

public class SumOfIntegers {

    public static int addIntegers(int[] integers) {
        int sum = 0;
        for (int nextInteger : integers) {
            if (nextInteger > 0 && (nextInteger > (Integer.MAX_VALUE - sum))) {
                return Integer.MAX_VALUE;
            } else if (nextInteger < 0 && (nextInteger < Integer.MIN_VALUE + sum)) {
                return Integer.MIN_VALUE;
            } else {
                sum += nextInteger;
            }
        }
        return sum;
    }
}


