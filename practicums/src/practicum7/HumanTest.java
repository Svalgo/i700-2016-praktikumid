package practicum7;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class HumanTest {

    @Test
    public void greetAdult() throws Exception {
        Human myHuman = new Human("John", 24);
        Assert.assertEquals("Hello, I am John and I am 24 years old.", myHuman.greetAsString());
    }

    @Test
    public void greetChild() throws Exception {
        Human myHuman = new Human("John", 2);
        Assert.assertEquals("Boo boo", myHuman.greetAsString());
    }



}