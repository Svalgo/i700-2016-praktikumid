package practicum7;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SumOfIntegersTest {

    @Test
    public void AddingIntegersResultsInCorrectValue() {
        int[] integers = {10000, 500000, 2000};
        int sum = SumOfIntegers.addIntegers(integers);
        Assert.assertEquals(512000, sum);
    }

    @Test
    public void AddingVeryLargeIntegersResultsInIntegerMaxValue() {
        int[] integers = {Integer.MAX_VALUE - 1, Integer.MAX_VALUE - 3};
        int sum = SumOfIntegers.addIntegers(integers);
        Assert.assertEquals(Integer.MAX_VALUE, sum);
    }

    @Test
    public void AddingVerySmallIntegersResultsInIntegerMinValue() {
        int[] integers = {Integer.MIN_VALUE + 5, Integer.MIN_VALUE + 8};
        int sum = SumOfIntegers.addIntegers(integers);
        Assert.assertEquals(Integer.MIN_VALUE, sum);
    }
}