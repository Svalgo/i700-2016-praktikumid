package practicum8;

/**
 * Created by mkunnapa on 17.10.2016.
 */
public enum Suit {
    DIAMONDS, CLUBS, SPADES, HEARTS;

    @Override
    public String toString() {
        char suite;
        switch (this) {
            case DIAMONDS:
                suite = '♦';
                break;
            case CLUBS:
                suite = '♣';
                break;
            case SPADES:
                suite = '♠';
                break;
            case HEARTS:
                suite = '♥';
                break;
            default:
                throw new IllegalStateException("No icon available for suit: " + super.toString());
        }
        return String.valueOf(suite);
    }
}
