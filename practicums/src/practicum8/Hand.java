package practicum8;

import java.util.ArrayList;

/**
 * Created by mkunnapa on 17.10.2016.
 */
public class Hand {
    private ArrayList<Card> cards = new ArrayList<>();
    private PokerRank highestRank = PokerRank.HIGH_CARD;

    public void add(Card card) {
        cards.add(card);
    }

    public void drawFromDeck(Deck deck) {
        cards.add(deck.draw());
    }

    public void showCards() {
        for (Card card : cards) {
            System.out.println(card);
        }
    }

    public void checkForPokerHand() {
        if (isRoyalFlush()) highestRank = PokerRank.ROYAL_FLUSH;
        else if (isStraightFlush()) highestRank = PokerRank.STRAIGHT_FLUSH;
        else if (isFourOfAKind()) highestRank = PokerRank.FOUR_OF_A_KIND;
        else if (isFullHouse()) highestRank = PokerRank.FULL_HOUSE;
        else if (isFlush()) highestRank = PokerRank.FLUSH;
        else if (isStraight()) highestRank = PokerRank.STRAIGHT;
        else if (isThreeOfAKind()) highestRank = PokerRank.THREE_OF_A_KIND;
        else if (isTwoPairs()) highestRank = PokerRank.TWO_PAIRS;
        else if (isPair()) highestRank = PokerRank.PAIR;
        else highestRank = PokerRank.HIGH_CARD;
    }

    public PokerRank getHighestRank() {
        return highestRank;
    }

    private boolean isRoyalFlush() {
        sortByRank();
        boolean isRoyal = cards.get(0).getRank() == Rank.ACE
                && cards.get(1).getRank() == Rank.KING
                && cards.get(2).getRank() == Rank.QUEEN
                && cards.get(3).getRank() == Rank.JACK
                && cards.get(4).getRank() == Rank.TEN;
        return isRoyal && isFlush();
    }

    private boolean isStraightFlush() {
        return isStraight() && isFlush();
    }

    private boolean isFourOfAKind() {
        sortByRank();
        boolean middleRanksEqual = cards.get(1).getRank() == cards.get(2).getRank()
                && cards.get(2).getRank() == cards.get(3).getRank();
        boolean firstTwoRanksEqual = cards.get(0).getRank() == cards.get(1).getRank();
        boolean lastTwoRanksEqual = cards.get(3).getRank() == cards.get(4).getRank();
        return (firstTwoRanksEqual || lastTwoRanksEqual) && middleRanksEqual;
    }

    private boolean isFullHouse() {
        sortByRank();
        boolean firstTwoRanksEqual = cards.get(0).getRank() == cards.get(1).getRank();
        boolean lastTwoRanksEqual = cards.get(3).getRank() == cards.get(4).getRank();
        boolean secondAndThirdRankEqual = cards.get(1).getRank() == cards.get(2).getRank();
        boolean thirdAndFourthRankEqual = cards.get(2).getRank() == cards.get(3).getRank();
        return firstTwoRanksEqual && lastTwoRanksEqual && (secondAndThirdRankEqual || thirdAndFourthRankEqual);
    }

    private boolean isFlush() {
        sortBySuit();
        return cards.get(0).getSuit() == cards.get(4).getSuit();
    }

    private boolean isStraight() {
        sortByRank();
        int highest = cards.get(0).getRank().getValue();
        int secondHighest = cards.get(1).getRank().getValue();
        int thirdHighest = cards.get(2).getRank().getValue();
        int fourthHighest = cards.get(3).getRank().getValue();
        int fifthHighest = cards.get(4).getRank().getValue();
        return highest - 1 == secondHighest
                && secondHighest - 1 == thirdHighest
                && thirdHighest - 1 == fourthHighest
                && fourthHighest - 1 == fifthHighest;
    }

    private boolean isThreeOfAKind() {
        sortByRank();
        int sameRank = 1;
        for (int i = 0; i < cards.size() - 1; i++) {
            if (cards.get(i).getRank() == cards.get(i + 1).getRank()) {
                sameRank++;
            } else {
                sameRank = 1;
            }
            if (sameRank == 3) return true;
        }
        return false;
    }

    private boolean isTwoPairs() {
        sortByRank();
        boolean firstTwoEqual = cards.get(0).getRank() == cards.get(1).getRank();
        boolean secondThirdEqual = cards.get(1).getRank() == cards.get(2).getRank();
        boolean threeAndFourEqual = cards.get(2).getRank() == cards.get(3).getRank();
        boolean fourAndFiveEqual = cards.get(3).getRank() == cards.get(4).getRank();
        return (firstTwoEqual && (threeAndFourEqual || fourAndFiveEqual)) || (secondThirdEqual && fourAndFiveEqual);
    }

    private  boolean isPair() {
        sortByRank();
        for (int i = 0; i < cards.size()-1; i++) {
            if (cards.get(i).getRank() == cards.get(i+1).getRank()) {
                return true;
            }
        }
        return false;
    }

    private void sortByRank() {
        //This is java 8 lambda syntax, don't worry if this syntax is confusing to you
        cards.sort((c1, c2) -> c1.getRank().getValue() - c2.getRank().getValue());
    }

    private void sortBySuit() {
        cards.sort((c1, c2) -> c1.getSuit().compareTo(c2.getSuit()));
    }


/*    public Card playCard(Card card) {
        //remove a card from your hand
        cards.get(card);
    }*/
}
