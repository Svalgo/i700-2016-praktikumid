import java.util.ArrayList;

public class ExamAnswer {
    public static void main(String[] args) {

//        Test reverse()
//        String blaa = reverse("kas aias sadas saia?");

//        Test revArray()
//        String[] inp = {"aab", "bb", "ccbbbbb", "dd"};
//        String blaa = revArray(inp);

//        Test checkInt()
//        boolean blaa = checkInt(-12423545);

//        Test average()
//        double blaa =  average(inp);

//        Test textFromMatrix()
        char[][] inp = {{'a','t','e'},
                {'d',' ','r'},
                {'o','p'},
                {'l','d','o','o'}};
        String blaa = textFromMatrix(inp);

//        Test removeSome()
//        String[] inp = {"one", "two", "three", "four", "mikk"};
//        ArrayList<String> blaa = removeSome(inp);

//        Test removeDecreasing()
//        int[] inp = {-3, 1, 2, 1, 5, 3, 4, 7, 5};
//        ArrayList<Integer> blaa = removeDecreasing(inp);

//        for (int b:blaa) {
//            System.out.println(b);
//        }
        System.out.println(blaa);
    }

    // write a method that creates a String from given two-dimensional array of characters.
    // Every word is in its own column, space characters found in the array must not be included.
    // Words must be separated by spaces. The rows of the two-dimensional character array might differ in lengths.

    public static String textFromMatrix(char[][] inpasdfg){
        String result = "";
        String[] temp = new String[inpasdfg.length];

        for (int i = 0; i < inpasdfg.length; i++) {
            char[] row = inpasdfg[i];
            for (int j = 0; j < row.length; j++) {
                if (row[j] != ' '){
                    if (temp[j] != null){
                        temp[j] += row[j];
                    } else {
                        temp[j] = ""+row[j];
                    }
                }
            }
        }

        for (int i = 0; i < temp.length; i++) {
            result += temp[i];
            if (i != temp.length - 1){
                result += " ";
            }
        }

        return result;
    }

    // write a method that reverses the order of elements in the given array.
    public static String revArray(String[] inp){
        String result = "";
        for (int i = inp.length - 1; i >= 0; i--) {
            result += inp[i];
        }

        return result;
    }

    // Write a method in java to check whether a given integer n is negative odd number.
    public static boolean checkInt(int inp){
        boolean result = false;

        if (inp < 0 && inp % 2 != 0){
            result = true;
        }

        return result;
    }

    // write a program that finds the arithmetic average of characters from a given array of words
    public static double average(String[] inp){
        double result;
        int sum = 0;

        for (int i = 0; i < inp.length; i++) {
            String word = inp[i];

            sum += word.length();
        }

        result = sum / (inp.length);
        return result;
    }

    // Write a program that reverses the order of words (only counting space as a separator) in a String
    public static String reverse(String tekst){
        ArrayList<String> tempArr = new ArrayList<String>();
        String temp = "";
        String result = "";


        for (int i = 0; i < tekst.length(); i++) {
            if (tekst.charAt(i) != ' '){
                if (i == tekst.length() -1){
                    temp += tekst.charAt(i);
                    tempArr.add(temp + ' ');
                    //temp = "";
                } else {
                    temp += tekst.charAt(i);
                }
            } else {
                if (tempArr.isEmpty()){
                    tempArr.add(temp);
                    temp = "";
                } else {
                    tempArr.add(temp + ' ');
                    temp = "";
                }
            }
        }

        for (int i = tempArr.size() - 1 ; i >= 0 ; i--) {
            result += tempArr.get(i);
        }

        return result;
    }

    // Write a program that would return an array of words from the given array of words,
    // so that all words containing two same kind of characters in a row are removed.
    public static ArrayList<String> removeSome(String[] words){
        ArrayList<String> result = new ArrayList<String>();
        boolean dub = false;
        char prev = ' ';

        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            for (int j = 0; j < word.length(); j++) {
                if (prev == word.charAt(j)){
                    dub = true;
                }
                prev = word.charAt(j);
            }
            if (dub){
                dub = false;
            } else {
                result.add(word);
            }
            prev = ' ';
        }

        return result;
    }

    // Write a method that would return from a given array all numbers that are not increasing
    public static ArrayList<Integer> removeDecreasing(int[] numbers){
        ArrayList<Integer> result = new ArrayList<Integer>();
        int prev = Integer.MIN_VALUE;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] >= prev){
                result.add(numbers[i]);
                prev = numbers[i];
            }
        }

        return result;
    }

}