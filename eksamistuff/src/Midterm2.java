/**
 * Created by svalgo on 05.01.2017.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.text.ParseException;

public class Midterm2 {
    public static void main(String[] args) {
        int[][] res = muster(9);
        //asenda *
        String s = "Tere, TUDENG, 1234!";
        String t = asenda(s); // "Tere**TUDENG*******"
        System.out.println(s + " > " + t);

        //sportsmans score
        System.out.println(score(new int[]{4, 1, 2, 3, 0})); // 9

        //veerusummad
        //int[] res = veeruSummad(new int[][]{{1, 2, 3}, {4, 5, 6}}); // {5, 7, 9}

    }

//*
//Write a method in Java to generate an integer matrix of size n x n
// (n is a parameter of the method) elements of which are calculated by finding the square
// of the sum of the row index and the column index of the element (indices start from zero).

    private static int[][] muster(int n) {

        int[][] res = new int[n][n];

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                int sum = col + row;
                int square = sum * sum;
                res[row][col] = square;

            }

        }
        return res;
    }


//* Write a Java method to replace all non-letters in a given strings by symbol '*'.
// The solution must use a loop.


    public static String asenda(String s) {
        int len = s.length();
        char ch = '*';
        char[] meh = s.toCharArray();
        for (int i = 0; i < len; i++) {
            if (!Character.isLetter(meh[i])) {
                meh[i] = ch;
            }
        }
        String puf = String.valueOf(meh);
        return puf;
    }

//*  Sportsmans score is calculated as sum of points from different attempts and two worst attempts
//   are not counted (there are more than two attempts).
//   Write a Java method to calculate the score if an array of points from all attempts is given.
//   Do not change the array given as parameter.


    public static int score(int[] points) {
        List<Integer> list = new ArrayList<Integer>();

        for (int i = 0; i < points.length; i++) {
            list.add(points[i]);
        }
        int[] lov = new int[2];
        Arrays.fill(lov, Integer.MAX_VALUE);

        for (int n : points) {
            if (n < lov[1]) {
                lov[1] = n;
                Arrays.sort(lov);
            }
        }
        for (int x : lov) {
            list.remove((Integer) x);
        }
        int sum = 0;
        for (int z = 0; z < list.size(); z++) {
            sum += list.get(z);
        }
        return sum;
    }
//*Write a method in Java to find the array of sums of columns of a given matrix of
// integers m (j-th element of the answer is the sum of elements of the j-th column
// in the matrix). Rows of m might be of different length.


    public static int[] veeruSummad(int[][] m) {


        return null;
    }

}


