public class AnswerSumOfHundredsTest {

    public static void main(String[] args) {
        System.out.println(
                sum(new int[] { 12, 7, -854, 4567 })); // answer must be 13 (0 + 0 + 8 + 5)
    }

    public static int sum(int[] m) {
        int count = 0;
        for (int i = 0; i <m.length ; i++) {
            int tempNumb = Math.abs(m[i]/100);
            System.out.println(tempNumb);
                if (tempNumb>9){
                    count = count + tempNumb%10;
                }else {
                    count = count + tempNumb;
                }

        }
        System.out.print("count: ");
        System.out.println(count);
        return count;
    }
}