public class AnswerAverageLength {

    public static void main(String[] args) {
        // should print out 3.0
        System.out.println(averageLength(new String[] {"this", "is", "fun", "pere"}));
        System.out.println(averageLength(new String[] {"to", "be", "or", "not", "to", "be"}));
        System.out.println(averageLength(new String[] {"", "", "", "", "", "", "", "ha"}));

    }

    public static double averageLength(String[] words) {
        double count = .0;


        for (int i = 0; i <words.length ; i++) {
            count = count + words[i].length();
            System.out.println(count);
        }
        double avg = count/(words.length);
        return avg;
    }

}
