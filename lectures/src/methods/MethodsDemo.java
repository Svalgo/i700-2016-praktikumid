package methods;

import lib.TextIO;

public class MethodsDemo {
    public static void main(String[] args) {
        //void method
        //greeting();

        //void with single parameter
        //greetUser("Teacher");

        //void with multiple parameters
        //saySomething("My name is", "Mihkel");

//        int eight = getFour() + 4;
//        System.out.println(eight);

//        greetUser(getMyName());
//        int x = 4;
//        int y = 5;
//        System.out.println(add(x,y));
//        writeXTimes(10, "I will study at home");
        writeXTimes(5, "And I will also read the course book");
//        writeXTimes(-1 , "LOL");
    }

    public static int add(int a, int b) {
        return a + b;
    }

    public static void writeXTimes(int howManyTimes, String whatYouAreWriting) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println(whatYouAreWriting);
            return;
        }
    }

    public static void greeting(){
        System.out.println("Hello, user!");
    }

    public static void greetUser(String userName) {
        System.out.println("Hello, " + userName);
    }

    public static void saySomething(String firstPart, String secondPart) {
        System.out.println(firstPart + " " + secondPart);
    }

    public static int getFour() {
        if (4 < 5) {
            return 4;
        }
        return -1;
    }

    public static String getMyName() {
        return "Mihkel";
    }

    public static void getUserInputs() {
        int num1 = TextIO.getInt();
        int num2 = TextIO.getInt();
        int num3 = TextIO.getInt();
    }

    public static int getNumberTwelwe() {
        return 12;
    }
}
