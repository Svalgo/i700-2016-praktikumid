package oop;

import java.awt.*;

/**
 * Created by mkunnapa on 17.10.2016.
 */
public abstract class Shape {
    Color color;
    abstract double area();

    public Color getColor() {
        return color;
    }
}
