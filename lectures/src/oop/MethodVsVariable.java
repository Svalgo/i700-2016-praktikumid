package oop;

public class MethodVsVariable {
    public static void main(String[] args) {
        int y = 2;
        Integer z = new Integer(2);
/*        add2ToInt(z);
        System.out.println(z);*/
        Sportsman sportsman = new Sportsman();
        sportsman.setName("John");
        sportsman.setBirthYear(1995);
        add2ToBirthYear(sportsman);
        System.out.println(sportsman.getBirthYear());

    }

    private static int add2(int x) {
        return x + 2;
    }

    private static void add2ToBirthYear(Sportsman sportsman) {
        sportsman.setBirthYear(sportsman.getBirthYear() + 2);
    }

}
